<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

	<UiMod name="XpStatus+G" version="1.3" date="10/11/2020" >
		
		<Author name="Likort, Alpha_Male and Johndon" email="alpha_male@speakeasy.net" />
		<Description text="Displays your Experience, Renown, Guild XP, and Money earned during sessions." />

		<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" />

		<WARInfo>
			<Categories>
				<Category name="RVR" />
				<Category name="COMBAT" />
				<Category name="OTHER" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name="MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>

		<Files>
			<File name="language\XpStatusLocalization.lua" />
			<File name="source\XpStatus.lua" />
			<File name="source\XpStatus.xml" />
			<File name="source\XpStatusWindowNub.lua" />
			<File name="source\XpStatusWindowNub.xml" />

		</Files>

		<SavedVariables>
			<SavedVariable name="XpStatus.Settings" global="false"/>
		</SavedVariables>
		
		<OnInitialize>
			<CreateWindow name="XpStatusWindow" show="true" />
			<CreateWindow name="XpStatusWindowNub" show="true" />
		</OnInitialize>

		</UiMod>

</ModuleFile>