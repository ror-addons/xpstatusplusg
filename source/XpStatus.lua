--	Title: XPStatus+G v1.2
--
--	Author: Likort, Lords of the Dead, http://www.lotd.org - Original author
-- 					Alpha_Male (alpha_male@speakeasy.net) - Current maintainer
--					Johndon - RoR update
--
--	Description:	Displays your Experience, Renown, Guild XP, and Money earned during sessions.
--
--	Features: * Multiple selectable tabs that display Experience, Renown, Guild XP and Money information for current session
-- 						* Tab window for chosen stat displays the following information:
-- 							* Current amount for the displayed stat for this session
-- 							* Amount for the displayed stat till the next level (for Level, Renown Rank, or Guild Rank)
-- 							* Amount till set quota (for Gold) is achieved
-- 							* Current trending for the displayed stat, amount being gained per hour
-- 							* Current trending for the displayed stat, how much time until the next level, rank, or quota is reached
--							* Show duration of current session
-- 						* Show/Hide main window button
-- 						* Right-click Options Context Menu that has the following features:
--							* Edit Gold Quota
--							* Set Show/Hide Toggle Button Color
--							* Set Window Opacity
--							* Reset Stat Tabs, either individually or all of them
--						* Window and toggle button can be positioned anywhere on the screen:
--							* Show/Hide toggle button movable via the layouteditor
--							* Main window movable in game or via the layouteditor
--						* Mouseover tooltip info for each tab
--						* Multiple Language Support (Need translations)
--
--	Files: \language\XpStatusLocalization.lua
--				 \source\XpStatus.lua
--	 			 \source\XpStatus.xml
--	 			 \source\XpStatusWindowNub.lua
--	 			 \source\XpStatusWindowNub.xml
--	 			 \XpStatus.mod
--	 			 \XpStatus_Install.txt
--	 			 \XpStatus_Readme.txt
--			
--	Version History:
--										1.3 Adds session duration to each tab.
--										1.2 Maintenance Update and New Features
--									 		 - New versioning scheme      
-- 											 - Added support for 1.3.1 WARInfo Categories and Careers (XpStatus.mod)
--											 - Added support for version info (XpStatus.mod)
--											 - Updated mod version information for 1.4.5
--											 - Directory structure reorganization - THIS REQUIRES DELETING OLD DIRECTORY WHEN INSTALLING!
--											 - Localization support
--											 - SavedVariables setup reworked
--											 - Loading success output to chat and debug windows
--											 - Right-click context menu rework
--											 	 - Context menu anchor fixes
--											   - Reorganized context menus and sub-menus
--											 	   - More commonly used options higher in list
--											 	   - Added seperators to main context menus and sub-menus
--											 	   - Updated Show/Hide Button Color options
--											 		   - Added a None or no tint option (default)
--											 		   - Added Blue and Green options 
--											 		   - Black option replaced with Dark Gray and is no longer a fallback option
--													 - Added a Reset to Defaults for resetting addon windows to original size, scales, and positions
--											 - Reworked Gold Quota Window and functionality
--											 	 - Increased window size
--											 	 - Textbox input only takes number input and backspace
--											 	 - Removed negative number checks
--											 	 - Added check for chat display text, takes into account whether Gold Quota is same or new
--											 - Reworked Opacity window
--											 	 - Increased window size to prevent text cutoff and make room for new features
--											 	 - Added slider tick marks on quarter marks
--											 	 - Added text values to slider tick marks
--											 	 - Window opens centered on the main window
--											 - Increased default Opacity value
--											 - Reworked tab buttons and functionality
--											 	 - The tabs now behave like ever other tab in the game
--											 	 - Increased tab sizes to fit text properly
--											 	 - Changed "Gold" tab title to "Money"
--											 	 - Added tab seperators
--											 	 - Updated Tooltips for mouseover on each tap
--											 - Increased window size to match tab size increase and space things better
--											 - Reworded some of the text descriptions
--											 - References and displayed text changed to for consistency with regards to "money" and "gold"
--											 - Removed click anywhere on window functionality that cycled through tabs
--											 - Added new header file format and associated information
--											 - Script cleanup and optimizations (.lua and .xml files)
--											 - Script reorganization and additional comments (.lua and .xml files)
--											 - Updated license information in header file to match that on Curse site (MIT license)
--											 - Readme and install file additions
--									 1.1.12 - Added Tabs to switch directly to each Page
--									 1.1.11 - Better handling of negative Gold values
--										 		  - Window movable without using Interface Editor, Lock/Unlock option
--													- Changed order of context menu items
--									 1.1.10 - Quota is now only compared to your Gold per Session value
--													- Reset Options have been renamed to be more consistent with Tab Names
--									 1.1.9  - Added Money Earned Tab. You can set a Gold Quota that you are working towards in the ContextMenu.
--									 1.1.8  - You can now reset each Tab individually, as well as all Tabs
--									 1.1.7b - Fixed a bug that caused the addon to stop working
--									 1.1.7  - Added ContextMenu
--														- Reset All
--														- Change the Color of the Show/Hide Button
--														- Edit Window Opacity
--													- ContextMenu is activated with Right Mousebutton
--										 			- Displayed Page is now being changed with Left Mousebutton
--									 1.1.6	- Main Window should now be placed by default right under the Hide Button
--										 			- You can toggle Hide Button Colors by Rightclicking on it
--													- Changed XpStatus+G Addon Path. It's not a bug, it's a feature.
--									 1.1.5  - Added Reset Button
--									 1.1.4	- Added Show/Hide Button
--									 1.1.3	- Minor Cosmetic Change
--									 1.1.2	- Fixed XP gain in current Session display when loading into the game for the first time being displayed wrong
--									 1.1.1	- Added Guild XP Page to Askanko's XpStatus WAR Addon Version 1.1
--
--  Supported Versions: Warhammer Online v1.4.5
--
--	Dependencies: None
--
--	Addon Compatability: None
--
--	Future Features: Continued updates for multiple language support.
--
--	Known Issues: None
--
--	Additional Credits: Original based on XPStatus by Askanko, http://war.curse.com/downloads/war-addons/details/xp_rp_tracker.aspx
--
--	Special Thanks:	EA/Mythic for a great game and for releasing the API specs
--								  The now defunct War Wiki (www.thewarwiki.com) which was a great source of knowledge for WAR mod development
--									www.curse.com and www.curseforge.com for hosting WAR mod files and projects
--								  Trouble INC guild of Badlands for testing, feedback, and for their support
--
--	License/Disclaimers: This program released under the MIT License.
--
--											 Copyright (c) 2012
--
--											 Permission is hereby granted, free of charge, to any person obtaining a copy
--											 of this software and associated documentation files (the "Software"), to deal
--											 in the Software without restriction, including without limitation the rights
--											 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
--											 copies of the Software, and to permit persons to whom the Software is
--											 furnished to do so, subject to the following conditions:
--											 
--											 The above copyright notice and this permission notice shall be included in
--											 all copies or substantial portions of the Software.
--											 
--											 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
--											 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
--											 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
--											 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
--											 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--											 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
--											 THE SOFTWARE.

if not XpStatus then
	XpStatus = {}
end

XpStatus.Title = L"XpStatus+G"
XpStatus.Version = 1.3

------------------------------------------------------------------
----  Local Variables
------------------------------------------------------------------

-- Localization Text
local LOC_TEXT
if not LOC_TEXT then
	LOC_TEXT = XpStatus.wstrings[SystemData.Settings.Language.active] 
end

-- Name shortcut variables
local windowName = "XpStatusWindow"
local nubName = "XpStatusWindowNub"

-- Default addon main window dimensions
local winWidth 		= 340
local winHeight 	= 300

-- Default nub dimensions (taken from EA_Window_QuestTrackerNub)
local nubWidth = 36
local nubHeight = 38

-- Default main window opacity
local defaultOpacity = 0.8

-- Initialize array and associated data
local LevelInformation = 
{
	XpPerHour   = 0;
	XpSession   = 0;
	XpToLevel   = 0;
	XpRank	    = 0;

	RpPerHour   = 0;
	RpSession   = 0;
	RpToLevel   = 0;
	RpRank      = 0;

	GXPPerHour  = 0;
	GXPSession  = 0;
	GXPToLevel  = 0;
	GXPRank	    = 0;
	GXPFirst    = 0;
	
	MoneyPerHour = 0;
	MoneySession = 0;
	GoldToLevel = 0;
	GoldFirst   = 0;
	QuotaMet    = 0;

	TimeRankUp  = 0;
	TimePlayedXP  = 0.00001; -- guard against divide by zero
	TimePlayedRP  = 0.00001; -- guard against divide by zero
	TimePlayedGXP  = 0.00001; -- guard against divide by zero
	TimePlayedMoney  = 0.00001; -- guard against divide by zero
}

-- Initialize variables

local UpdateTime = 
{
	NextUpdate = 0;
	Frequency  = 5;
}

-- Define category indicies

local PVE = 0
local RVR = 1
local GXP = 2
local Money = 3

------------------------------------------------------------------
----  Misc Global/Local Variable Initializations and Functions
------------------------------------------------------------------

-- Define tab information

XpStatus.TABS_PVE = 1
XpStatus.TABS_RVR = 2
XpStatus.TABS_GUILD = 3
XpStatus.TABS_MONEY = 4

XpStatus.SelectedTab = XpStatus.TABS_PVE

XpStatus.Tabs = {}
XpStatus.Tabs[ XpStatus.TABS_PVE  ]	= {	window = windowName .."TabsXP", tooltip=LOC_TEXT.TOOLTIP_TAB_PVE }
XpStatus.Tabs[ XpStatus.TABS_RVR  ]	= { window = windowName.."TabsRP", tooltip=LOC_TEXT.TOOLTIP_TAB_RVR }
XpStatus.Tabs[ XpStatus.TABS_GUILD  ]	= { window = windowName.."TabsGXP", tooltip=LOC_TEXT.TOOLTIP_TAB_GUILD }
XpStatus.Tabs[ XpStatus.TABS_MONEY  ]	= { window = windowName.."TabsMoney", tooltip=LOC_TEXT.TOOLTIP_TAB_MONEY }

-- Define context menu anchors
XpStatus.ContextMenuAnchor = {
            Point = "topleft",
            RelativePoint = "topright", 
            RelativeTo = windowName,
            XOffset = 0, 
            YOffset = 2 }

XpStatus.OldGoldQuota = nil

------------------------------------------------------------------
----  Core Functions
------------------------------------------------------------------

function XpStatus.OnInitialize()
		
		-- Display mod info and loading confirmation
		TextLogAddEntry( "Chat", SystemData.ChatLogFilters.SHOUT, L"["..XpStatus.Title..L"]: "..L" Version "..(wstring.format( L"%.01f",XpStatus.Version))..L" Initialized" )
		DEBUG ( towstring( "XpStatus+G loaded" ))

		-- Options settings
		if not XpStatus.Settings then

				-- Initialize SavedVariables
				XpStatus.Settings = {
	    			Version = XpStatus.Version,
	    			currentMode = PVE,
	    			currentOpacity = defaultOpacity,
	    			GoldQuota = 0,
	    			nubColorFlag = 0,
	  		}

				-- Reset the entire addon
				XpStatus.ResetWindow()

		end

		-- Update the saved data version
		if (XpStatus.Settings.Version < XpStatus.Version) then
				XpStatus.Settings.Version = XpStatus.Version
		end

		if (XpStatus.OldGoldQuota == nil) then
			XpStatus.OldGoldQuota = 0
		end

		-- Set the window alpha based on saved settings
		WindowSetAlpha(windowName, XpStatus.Settings.currentOpacity)

		-- Register events 
		WindowRegisterEventHandler(windowName, SystemData.Events.PLAYER_EXP_UPDATED, "XpStatus.UpdateXP")
		WindowRegisterEventHandler(windowName, SystemData.Events.PLAYER_RENOWN_UPDATED, "XpStatus.UpdateRP")
		WindowRegisterEventHandler(windowName, SystemData.Events.GUILD_EXP_UPDATED, "XpStatus.UpdateGXP")
		WindowRegisterEventHandler(windowName, SystemData.Events.PLAYER_MONEY_UPDATED, "XpStatus.UpdateMoney" )

		WindowRegisterEventHandler(windowName, SystemData.Events.WORLD_OBJ_XP_GAINED, "XpStatus.AddXP" )
		WindowRegisterEventHandler(windowName, SystemData.Events.WORLD_OBJ_RENOWN_GAINED, "XpStatus.AddRP" )

		-- Register the window with the layout editor to allow it to be customized
		LayoutEditor.RegisterWindow(
	        windowName,           					-- window name internal
	        L"XpStatus",             				-- window display name
	        L"Displays XP RP GXP Money",   	-- description
	        true, true,                     -- allow resize width, height
	        true,                           -- allow hide
	        nil)                            -- hidden callback

		-- Initialize and set control properties for main window
		ButtonSetText(windowName.."TabsXP", GetString( StringTables.Default.LABEL_PERFORMANCE_PVE) )
		ButtonSetText(windowName.."TabsRP", GetString( StringTables.Default.LABEL_PERFORMANCE_RVR) )
		ButtonSetText(windowName.."TabsGXP", GetString( StringTables.Default.LABEL_GUILD) )
		ButtonSetText(windowName.."TabsMoney", GetString( StringTables.Default.LABEL_MONEY)  )

		LevelInformation.GXPFirst = GameData.Guild.m_GuildExpCurrent
		LevelInformation.GoldFirst = GameData.Player.money
	
		XpStatus.UpdateXP()	
		XpStatus.UpdateRP()
		XpStatus.UpdateGXP()
		XpStatus.UpdateMoney()
	
		XpStatus.ChangeDisplayMode()

		-- Initialize and set control properties for quota editing window
		XpStatus.InitializeQuotaWindow()
		
		-- Initialize and set control properties for set opacity window
		XpStatus.InitializeSetOpacityWindow()

end

function XpStatus.InitializeQuotaWindow()

		-- Create the window
		CreateWindow("XpStatusQuotaWindow", false)
		
		LabelSetText("XpStatusQuotaWindowLabel", LOC_TEXT.TEXT_ENTER_QUOTA )
		ButtonSetText("XpStatusQuotaWindowSetButton", LOC_TEXT.TEXT_SET )
		ButtonSetText("XpStatusQuotaWindowCancelButton", GetString( StringTables.Default.LABEL_CANCEL ) )
		TextEditBoxSetText("XpStatusQuotaWindowText", towstring(XpStatus.Settings.GoldQuota))
end

function XpStatus.InitializeSetOpacityWindow()

		-- Create the window
		CreateWindow("XpStatusSetOpacityWindow", false)

		LabelSetText( "XpStatusSetOpacityWindowTitleBarText", GetString( StringTables.Default.LABEL_OPACITY ))
		
		LabelSetText( "XpStatusSetOpacityWindowSliderMinLabel", L"0.0" )
		LabelSetText( "XpStatusSetOpacityWindowSliderMaxLabel", L"1.0" )
		LabelSetText( "XpStatusSetOpacityWindowSliderMidLabel", L"0.5" )
		
		-- Set the slider bar position and value based on saved settings
		SliderBarSetCurrentPosition("XpStatusSetOpacityWindowSlider", XpStatus.Settings.currentOpacity )

end

function XpStatus.ChangeDisplayMode()

		-- Define local string variable for Stat
		local strStat
	
		if (XpStatus.Settings.currentMode == PVE) then
			strStat = LOC_TEXT.TEXT_XP
			XpStatus.SelectedTab = XpStatus.TABS_PVE
		elseif (XpStatus.Settings.currentMode == RVR) then
			strStat = LOC_TEXT.TEXT_RP
			XpStatus.SelectedTab = XpStatus.TABS_RVR
		elseif (XpStatus.Settings.currentMode == GXP) then
			strStat = LOC_TEXT.TEXT_GXP
			XpStatus.SelectedTab = XpStatus.TABS_GUILD
		elseif (XpStatus.Settings.currentMode == Money) then
			strStat = GetString( StringTables.Default.LABEL_MONEY )
			XpStatus.SelectedTab = XpStatus.TABS_MONEY
		else
			TextLogAddEntry("Chat", 0, LOC_TEXT.ERROR_MESSAGE01)
	
			XpStatus.Settings.currentMode = PVE
	
			strStat = LOC_TEXT.TEXT_XP
			XpStatus.SelectedTab = XpStatus.TABS_PVE
		end		

		-- Modify the tab button
		XpStatus.SetHighlightedTab(XpStatus.SelectedTab)

		-- Set display text
		LabelSetText(windowName.."StaticPerSession", strStat .. LOC_TEXT.TEXT_SESSION)
		if (XpStatus.Settings.currentMode == Money) then
			LabelSetText(windowName.."StaticToLevel", LOC_TEXT.TEXT_UNTIL .. XpStatus.Settings.GoldQuota .. L" " .. GetMailString( StringTables.Mail.LABEL_MAIL_COIN_GOLD ) .. L":" )
		else
			LabelSetText(windowName.."StaticToLevel", strStat .. LOC_TEXT.TEXT_NEXT_LEVEL)
		end

		LabelSetText(windowName.."StaticPerHour", strStat .. LOC_TEXT.TEXT_PER_HOUR)
		if (XpStatus.Settings.currentMode == Money) then
			LabelSetText(windowName.."StaticTimeRankUp", LOC_TEXT.TEXT_RANKUP .. XpStatus.Settings.GoldQuota .. L" " .. GetMailString( StringTables.Mail.LABEL_MAIL_COIN_GOLD ) .. L":" )
		else
			LabelSetText(windowName.."StaticTimeRankUp",LOC_TEXT.TEXT_RANKUP)
		end
	
		XpStatus.UpdateTimed()

end

function XpStatus.AddXP(_, amount)
		LevelInformation.XpSession = LevelInformation.XpSession + amount
		XpStatus.DisplayStatistic()
end

function XpStatus.AddRP(_, amount)
		LevelInformation.RpSession = LevelInformation.RpSession + amount
		XpStatus.DisplayStatistic()
end

function XpStatus.UpdateXP()
		local totalEarned = GameData.Player.Experience.curXpEarned
		local totalNeeded = GameData.Player.Experience.curXpNeeded
	
		if (totalNeeded == nil) then
			return
		end
	
		LevelInformation.XpToLevel = totalNeeded - totalEarned
	
		XpStatus.DisplayStatistic()
end

function XpStatus.UpdateRP()
		local totalEarned = GameData.Player.Renown.curRenownEarned
		local totalNeeded = GameData.Player.Renown.curRenownNeeded
	
		if (totalNeeded == nil) then
			return
		end
	
		LevelInformation.RpToLevel = totalNeeded - totalEarned
		XpStatus.DisplayStatistic()
end

function XpStatus.UpdateGXP()
		local totalEarned = GameData.Guild.m_GuildExpCurrent
		local totalNeeded = GameData.Guild.m_GuildExpNeeded
	
		if (totalNeeded == nil) then
			return
		end
	
		LevelInformation.GXPToLevel = totalNeeded - totalEarned
	
		LevelInformation.GXPSession = totalEarned - LevelInformation.GXPFirst
	
		XpStatus.DisplayStatistic()
end

function XpStatus.UpdateMoney()
		local totalEarned = GameData.Player.money
		local totalNeeded = XpStatus.Settings.GoldQuota*10000	
		
		if (totalNeeded == nil) then
			return
		end
		
		LevelInformation.MoneySession = totalEarned - LevelInformation.GoldFirst
	
		LevelInformation.GoldToLevel = totalNeeded - LevelInformation.MoneySession
		
		if (LevelInformation.MoneySession >= XpStatus.Settings.GoldQuota*10000) and (LevelInformation.QuotaMet == 0) then
			Sound.Play(201)
			TextLogAddEntry("Chat", 0, LOC_TEXT.MESSAGE_GOLD_QUOTA_REACHED)
			LevelInformation.QuotaMet = 1
		end
	
		XpStatus.DisplayStatistic()
end

function XpStatus.UpdateTimed()
		
		-- Guild.m_GuildExpCurrent reports a low value when the Game is being loaded
		if (LevelInformation.GXPFirst < 10) then
			LevelInformation.GXPFirst    = GameData.Guild.m_GuildExpCurrent
			XpStatus.UpdateGXP()
		end
	
		-- same for GameData.Player.money
		if (LevelInformation.GoldFirst < 1) then
			LevelInformation.GoldFirst    = GameData.Player.money
			XpStatus.UpdateMoney()
		end
	
		LevelInformation.XpPerHour = math.floor(LevelInformation.XpSession * 3600 / LevelInformation.TimePlayedXP)
		LevelInformation.RpPerHour = math.floor(LevelInformation.RpSession * 3600 / LevelInformation.TimePlayedRP)
		LevelInformation.GXPPerHour = math.floor(LevelInformation.GXPSession * 3600 / LevelInformation.TimePlayedGXP)
		LevelInformation.MoneyPerHour = math.floor(LevelInformation.MoneySession * 3600 / LevelInformation.TimePlayedMoney)
	
		if (XpStatus.Settings.currentMode == PVE) then
	
			if (LevelInformation.XpPerHour == 0) then
				LevelInformation.TimeRankUp = 0
			else
				LevelInformation.TimeRankUp = math.floor(LevelInformation.XpToLevel / (LevelInformation.XpPerHour / 3600))
			end  
		elseif (XpStatus.Settings.currentMode == RVR) then
			if (LevelInformation.RpPerHour == 0) then
				LevelInformation.TimeRankUp = 0
			else
				LevelInformation.TimeRankUp = math.floor(LevelInformation.RpToLevel / (LevelInformation.RpPerHour / 3600))
			end  
		elseif (XpStatus.Settings.currentMode == GXP) then
			if (LevelInformation.GXPPerHour == 0) then
				LevelInformation.TimeRankUp = 0
			else
				LevelInformation.TimeRankUp = math.floor(LevelInformation.GXPToLevel / (LevelInformation.GXPPerHour / 3600))
			end
		elseif (XpStatus.Settings.currentMode == Money) then
			if (LevelInformation.MoneyPerHour == 0) then
				LevelInformation.TimeRankUp = 0
			else
				LevelInformation.TimeRankUp = math.floor(LevelInformation.GoldToLevel / (LevelInformation.MoneyPerHour / 3600))
			end
		else
			TextLogAddEntry("Chat", 0, LOC_TEXT.ERROR_MESSAGE02)
	
			XpStatus.Settings.currentMode = PVE
	
			if (LevelInformation.XpPerHour == 0) then
				LevelInformation.TimeRankUp = 0
			else
				LevelInformation.TimeRankUp = math.floor(LevelInformation.XpToLevel / (LevelInformation.XpPerHour / 3600))
			end
	  
		end
		XpStatus.DisplayStatistic()
end

function XpStatus.DisplayStatistic()
	
		if (XpStatus.Settings.currentMode == PVE) then
			local ltime, _, _ = TimeUtils.FormatClock(LevelInformation.TimePlayedXP)
			LabelSetText(windowName.."TimePlayed", L""..ltime)
			LabelSetText(windowName.."PerSession", L""..LevelInformation.XpSession)
			LabelSetText(windowName.."ToLevel", L""..LevelInformation.XpToLevel)
			LabelSetText(windowName.."PerHour", L""..LevelInformation.XpPerHour)
	
			if GameData.Player and GameData.Player.level then
				LabelSetText(windowName.."TitleBarText", LOC_TEXT.TITLE_PVE_RANK .. GameData.Player.level)
			else
				LabelSetText(windowName.."TitleBarText", LOC_TEXT.TITLE_PVE_RANK_NA)	
			end 
	
		elseif (XpStatus.Settings.currentMode == RVR) then
			local ltime, _, _ = TimeUtils.FormatClock(LevelInformation.TimePlayedRP)
			LabelSetText(windowName.."TimePlayed", L""..ltime)
			LabelSetText(windowName.."PerSession", L""..LevelInformation.RpSession)
			LabelSetText(windowName.."ToLevel", L""..LevelInformation.RpToLevel)
			LabelSetText(windowName.."PerHour", L""..LevelInformation.RpPerHour)
	
			if GameData.Player and GameData.Player.Renown  and GameData.Player.Renown.curRank then
				LabelSetText(windowName.."TitleBarText", LOC_TEXT.TITLE_RVR_RANK .. GameData.Player.Renown.curRank)
			else
				LabelSetText(windowName.."TitleBarText", LOC_TEXT.TITLE_RVR_RANK_NA)	
			end 
	
		elseif (XpStatus.Settings.currentMode == GXP) then
			local ltime, _, _ = TimeUtils.FormatClock(LevelInformation.TimePlayedGXP)
			LabelSetText(windowName.."TimePlayed", L""..ltime)
			LabelSetText(windowName.."PerSession", L""..LevelInformation.GXPSession)
			LabelSetText(windowName.."ToLevel", L""..LevelInformation.GXPToLevel)
			LabelSetText(windowName.."PerHour", L""..LevelInformation.GXPPerHour)
	
			if GameData.Guild and GameData.Guild.m_GuildRank then
				LabelSetText(windowName.."TitleBarText", LOC_TEXT.TITLE_GUILD_RANK .. GameData.Guild.m_GuildRank)
			else
				LabelSetText(windowName.."TitleBarText", LOC_TEXT.TEXT_NO_GUILD_RANK)	
			end 
	
		elseif (XpStatus.Settings.currentMode == Money) then
			local ltime, _, _ = TimeUtils.FormatClock(LevelInformation.TimePlayedMoney)
			LabelSetText(windowName.."TimePlayed", L""..ltime)
			LabelSetText(windowName.."PerSession", L""..XpStatus.GoldDisplay(LevelInformation.MoneySession))
	
			if (LevelInformation.MoneySession >= XpStatus.Settings.GoldQuota*10000) then
				LabelSetText(windowName.."ToLevel", LOC_TEXT.TEXT_QUOTA_REACHED)
			else
				LabelSetText(windowName.."ToLevel", L""..XpStatus.GoldDisplay(LevelInformation.GoldToLevel))
			end
			LabelSetText(windowName.."PerHour", L""..XpStatus.GoldDisplay(LevelInformation.MoneyPerHour))
	
			LabelSetText(windowName.."StaticToLevel", LOC_TEXT.TEXT_UNTIL .. XpStatus.Settings.GoldQuota .. L" " .. GetMailString( StringTables.Mail.LABEL_MAIL_COIN_GOLD ) .. L":" )
			LabelSetText(windowName.."StaticTimeRankUp", LOC_TEXT.TEXT_UNTIL .. XpStatus.Settings.GoldQuota .. L" " .. GetMailString( StringTables.Mail.LABEL_MAIL_COIN_GOLD ) .. L":" )
	
			LabelSetText(windowName.."TitleBarText", LOC_TEXT.TEXT_MONEY_EARNED)
	
		else
			TextLogAddEntry("Chat", 0, LOC_TEXT.ERROR_MESSAGE03)
	
			XpStatus.Settings.currentMode = PVE
	
			local ltime, _, _ = TimeUtils.FormatClock(LevelInformation.TimePlayedXP)
			LabelSetText(windowName.."TimePlayed", L""..ltime)
			LabelSetText(windowName.."PerSession", L""..LevelInformation.XpSession)
			LabelSetText(windowName.."ToLevel", L""..LevelInformation.XpToLevel)
			LabelSetText(windowName.."PerHour", L""..LevelInformation.XpPerHour)
	
			if GameData.Player and GameData.Player.level then
				LabelSetText(windowName.."TitleBarText", LOC_TEXT.TITLE_PVE_RANK .. GameData.Player.level)
			else
				LabelSetText(windowName.."TitleBarText", LOC_TEXT.TITLE_PVE_RANK_NA)	
			end 
		end

		if (LevelInformation.TimeRankUp == 0 or LevelInformation.TimeRankUp >= 360000) then
			LabelSetText(windowName.."TimeRankUp", LOC_TEXT.TEXT_NA)
		else
			local ltime, _, _ = TimeUtils.FormatClock(LevelInformation.TimeRankUp)
			LabelSetText(windowName.."TimeRankUp", L""..ltime)
		end

end

function XpStatus.ChangeModeTo(mode)

		if (mode == PVE) then
			XpStatus.Settings.currentMode = PVE
		elseif (mode == RVR) then
			XpStatus.Settings.currentMode = RVR
		elseif (mode == GXP) then
			XpStatus.Settings.currentMode = GXP
		elseif (mode == Money) then
			XpStatus.Settings.currentMode = Money
		else
			XpStatus.Settings.currentMode = PVE
		end
	
		XpStatus.ChangeDisplayMode()
end

-- Reset the addon window and settings to default
function XpStatus.ResetWindow()
		
		if DoesWindowExist(windowName) then
				-- Reset main window dimensions to original
				WindowSetDimensions( windowName, winWidth, winHeight )
				
				-- Reset main window position to center screen
				WindowRestoreDefaultSettings (windowName)
				WindowSetScale(windowName, InterfaceCore.GetScale())
				
				-- Reset window opacity
				XpStatus.Settings.currentOpacity = defaultOpacity   			
				WindowSetAlpha(windowName, XpStatus.Settings.currentOpacity)
		end

		if DoesWindowExist(nubName) then
				-- Reset nub dimensions to original
				WindowSetDimensions( nubName, nubWidth, nubHeight )			

				-- Reset nub position to center screen
				WindowRestoreDefaultSettings (nubName)
				WindowSetScale(nubName, InterfaceCore.GetScale())
		
				-- Reset nub default color
				XpStatus.SetNubColorDefaultNone()
		end

end

function XpStatus.Shutdown()
		WindowUnRegisterEventHandler(windowName, SystemData.Events.PLAYER_EXP_UPDATED)
		WindowUnRegisterEventHandler(windowName, SystemData.Events.PLAYER_RENOWN_UPDATED)
		WindowUnRegisterEventHandler(windowName, SystemData.Events.WORLD_OBJ_XP_GAINED)
		WindowUnRegisterEventHandler(windowName, SystemData.Events.WORLD_OBJ_RENOWN_GAINED)
		WindowUnRegisterEventHandler(windowName, SystemData.Events.GUILD_EXP_UPDATED)
		WindowUnRegisterEventHandler(windowName, SystemData.Events.PLAYER_MONEY_UPDATED)
end

function XpStatus.Update(timePassed)
		-- Update player data
		LevelInformation.TimePlayedXP = LevelInformation.TimePlayedXP + timePassed
		LevelInformation.TimePlayedRP = LevelInformation.TimePlayedRP + timePassed
		LevelInformation.TimePlayedGXP = LevelInformation.TimePlayedGXP + timePassed
		LevelInformation.TimePlayedMoney = LevelInformation.TimePlayedMoney + timePassed
		
		UpdateTime.NextUpdate = UpdateTime.NextUpdate - timePassed
		
		if (UpdateTime.NextUpdate < 0) then
			UpdateTime.NextUpdate = UpdateTime.Frequency
			XpStatus.UpdateTimed()
		end
end

function XpStatus.ToggleShowing()
		WindowSetShowing( windowName, not WindowGetShowing( windowName) )
end 

function XpStatus.Show()
		WindowSetShowing( windowName, true)
end

function XpStatus.Hide()
		WindowSetShowing( windowName, false)	
end

function XpStatus.GoldDisplay(goldToDisplay)
		if (goldToDisplay < 0) then
			goldToDisplay = 0
		end
		
		local g = math.floor (goldToDisplay / 10000)
		local s = math.floor ((goldToDisplay - (g * 10000)) / 100)
		local b = math.mod (goldToDisplay, 100)
	
		-- TODO: show icons instead?
		local gString = g .. LOC_TEXT.TEXT_SHORT_GOLD .. L" " .. s .. LOC_TEXT.TEXT_SHORT_SILVER .. L" " .. b .. LOC_TEXT.TEXT_SHORT_BRONZE
	
		return gString
end


------------------------------------------------------------------
---- Context Menus
------------------------------------------------------------------

function XpStatus.RightClick()

		local movable = WindowGetMovable( windowName )

		EA_Window_ContextMenu.CreateContextMenu( "XpStatus", EA_Window_ContextMenu.CONTEXT_MENU_1 )
		EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_TO_LOCK ), XpStatus.OnLock, not movable, true, EA_Window_ContextMenu.CONTEXT_MENU_1 )
		EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_TO_UNLOCK ), XpStatus.OnUnlock, movable, true, EA_Window_ContextMenu.CONTEXT_MENU_1 )   
		EA_Window_ContextMenu.AddMenuDivider( EA_Window_ContextMenu.CONTEXT_MENU_1 )
		EA_Window_ContextMenu.AddMenuItem( LOC_TEXT.OPTIONS_EDIT_GOLD_QUOTA, XpStatus.ShowQuotaWindow, false, EA_Window_ContextMenu.CONTEXT_MENU_1 )
		EA_Window_ContextMenu.AddCascadingMenuItem( LOC_TEXT.OPTIONS_EDIT_BUTTON_COLOR, XpStatus.SpawnColorSelectionMenu, false, EA_Window_ContextMenu.CONTEXT_MENU_1 )
		EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_SET_OPACITY ), XpStatus.EditOpacity, false, true, EA_Window_ContextMenu.CONTEXT_MENU_1 )
		EA_Window_ContextMenu.AddMenuDivider( EA_Window_ContextMenu.CONTEXT_MENU_1 )
		EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_CHAT_OPTIONS_RESET ), XpStatus.ResetWindow, false, EA_Window_ContextMenu.CONTEXT_MENU_1 )
		EA_Window_ContextMenu.AddMenuDivider( EA_Window_ContextMenu.CONTEXT_MENU_1 )
		EA_Window_ContextMenu.AddCascadingMenuItem( LOC_TEXT.OPTIONS_RESET_TABS, XpStatus.SpawnResetSelectionMenu, false, EA_Window_ContextMenu.CONTEXT_MENU_1 )
		EA_Window_ContextMenu.Finalize( EA_Window_ContextMenu.CONTEXT_MENU_1, XpStatus.ContextMenuAnchor )
end

function XpStatus.OnLock()
	  WindowSetMovable(  windowName, false )
end

function XpStatus.OnUnlock()
	  WindowSetMovable(  windowName, true )
end

function XpStatus.SpawnColorSelectionMenu()
	
		EA_Window_ContextMenu.Hide( EA_Window_ContextMenu.CONTEXT_MENU_3 )
		EA_Window_ContextMenu.CreateContextMenu( "ColorSelectionMenu", EA_Window_ContextMenu.CONTEXT_MENU_2 )
		EA_Window_ContextMenu.AddMenuItem( LOC_TEXT.OPTIONS_COLOR_NONE .. L" ("..  GetStringFromTable( "CustomizeUiStrings", StringTables.CustomizeUi.LABEL_DEFAULT ) .. L")", XpStatus.SetNubColorDefaultNone, XpStatus.Settings.nubColorFlag == 0, true, EA_Window_ContextMenu.CONTEXT_MENU_2 )
		EA_Window_ContextMenu.AddMenuItem( LOC_TEXT.OPTIONS_COLOR_RED, XpStatus.SetNubColorRed, XpStatus.Settings.nubColorFlag == 1, true, EA_Window_ContextMenu.CONTEXT_MENU_2 )
		EA_Window_ContextMenu.AddMenuItem( LOC_TEXT.OPTIONS_COLOR_GREEN, XpStatus.SetNubColorGreen, XpStatus.Settings.nubColorFlag == 2, true, EA_Window_ContextMenu.CONTEXT_MENU_2 )
		EA_Window_ContextMenu.AddMenuItem( LOC_TEXT.OPTIONS_COLOR_BLUE, XpStatus.SetNubColorBlue, XpStatus.Settings.nubColorFlag == 3, true, EA_Window_ContextMenu.CONTEXT_MENU_2 )
		EA_Window_ContextMenu.AddMenuItem( LOC_TEXT.OPTIONS_COLOR_YELLOW, XpStatus.SetNubColorYellow, XpStatus.Settings.nubColorFlag == 4, true, EA_Window_ContextMenu.CONTEXT_MENU_2 )
		EA_Window_ContextMenu.AddMenuItem( LOC_TEXT.OPTIONS_COLOR_DARK_GRAY, XpStatus.SetNubColorDarkGray, XpStatus.Settings.nubColorFlag == 5, true, EA_Window_ContextMenu.CONTEXT_MENU_2 )
		EA_Window_ContextMenu.Finalize( EA_Window_ContextMenu.CONTEXT_MENU_2 )

end

function XpStatus.SetNubColorDefaultNone()
		XpStatus.Settings.nubColorFlag = 0
		WindowSetTintColor(nubName, DefaultColor.ZERO_TINT.r, DefaultColor.ZERO_TINT.g, DefaultColor.ZERO_TINT.b)
end

function XpStatus.SetNubColorRed()
		XpStatus.Settings.nubColorFlag = 1
		WindowSetTintColor(nubName, DefaultColor.RED.r, DefaultColor.RED.g, DefaultColor.RED.b)
end

function XpStatus.SetNubColorGreen()
		XpStatus.Settings.nubColorFlag = 2
		WindowSetTintColor(nubName, DefaultColor.GREEN.r, DefaultColor.GREEN.g, DefaultColor.GREEN.b)
end

function XpStatus.SetNubColorBlue()
		XpStatus.Settings.nubColorFlag = 3
		WindowSetTintColor(nubName, DefaultColor.BLUE.r, DefaultColor.BLUE.g, DefaultColor.BLUE.b)
end

function XpStatus.SetNubColorYellow()
		XpStatus.Settings.nubColorFlag = 4
		WindowSetTintColor(nubName, DefaultColor.YELLOW.r, DefaultColor.YELLOW.g, DefaultColor.YELLOW.b)
end

function XpStatus.SetNubColorDarkGray()
		XpStatus.Settings.nubColorFlag = 5
		WindowSetTintColor(nubName, DefaultColor.DARK_GRAY.r, DefaultColor.DARK_GRAY.g, DefaultColor.DARK_GRAY.b)
end

function XpStatus.EditOpacity()
    SliderBarSetCurrentPosition("XpStatusSetOpacityWindowSlider", XpStatus.Settings.currentOpacity )
  
    WindowClearAnchors( "XpStatusSetOpacityWindow" )
    WindowAddAnchor( "XpStatusSetOpacityWindow", "center", windowName, "center", 0 , 0 )

    WindowSetShowing( "XpStatusSetOpacityWindow", true )
end

function XpStatus.OnOpacitySlide( slidePos )
    XpStatus.Settings.currentOpacity = slidePos
    WindowSetAlpha(windowName, slidePos)
end

function XpStatus.OnCancelSetOpacity()
		WindowSetShowing( "XpStatusSetOpacityWindow", false )
end

function XpStatus.SpawnResetSelectionMenu()

		EA_Window_ContextMenu.Hide(EA_Window_ContextMenu.CONTEXT_MENU_2)
		EA_Window_ContextMenu.CreateContextMenu("ResetSelectionMenu", EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.AddMenuItem(LOC_TEXT.OPTIONS_RESET_ALL_TABS, XpStatus.resetAll, false, true, EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.AddMenuDivider (EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.AddMenuItem(LOC_TEXT.OPTIONS_RESET_PVE_TAB, XpStatus.resetXP, false, true, EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.AddMenuItem(LOC_TEXT.OPTIONS_RESET_RVR_TAB, XpStatus.resetRP, false, true, EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.AddMenuItem(LOC_TEXT.OPTIONS_RESET_GUILD_TAB, XpStatus.resetGXP, false, true, EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.AddMenuItem(LOC_TEXT.OPTIONS_RESET_MONEY_TAB, XpStatus.resetMoney, false, true, EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_3)

end

------------------------------------------------------------------
---- Reset Value Functions
------------------------------------------------------------------

function XpStatus.resetAll()
		LevelInformation.XpPerHour   = 0
		LevelInformation.XpSession   = 0
		LevelInformation.XpToLevel   = 0
		LevelInformation.XpRank	     = 0
	
		LevelInformation.RpPerHour   = 0
		LevelInformation.RpSession   = 0
		LevelInformation.RpToLevel   = 0
		LevelInformation.RpRank      = 0
	
		LevelInformation.GXPPerHour  = 0
		LevelInformation.GXPSession  = 0
		LevelInformation.GXPToLevel  = 0
		LevelInformation.GXPRank     = 0
	
		LevelInformation.MoneyPerHour = 0
		LevelInformation.MoneySession = 0
		LevelInformation.GoldToLevel = 0
		LevelInformation.GoldFirst   = 0
		LevelInformation.QuotaMet    = 0
	
	
		LevelInformation.TimePlayedXP  = 0.00001 -- guard against divide by zero
		LevelInformation.TimePlayedRP  = 0.00001 -- guard against divide by zero
		LevelInformation.TimePlayedGXP  = 0.00001 -- guard against divide by zero
		LevelInformation.TimePlayedMoney  = 0.00001 -- guard against divide by zero
	
		LevelInformation.GXPFirst = GameData.Guild.m_GuildExpCurrent
		LevelInformation.GoldFirst = GameData.Player.money
	
		XpStatus.UpdateXP()
		XpStatus.UpdateRP()
		XpStatus.UpdateGXP()
		XpStatus.UpdateMoney()
	
		XpStatus.ChangeDisplayMode()
end

function XpStatus.resetXP()
		LevelInformation.XpPerHour   = 0
		LevelInformation.XpSession   = 0
		LevelInformation.XpToLevel   = 0
		LevelInformation.XpRank	     = 0
	
		LevelInformation.TimePlayedXP  = 0.00001 -- guard against divide by zero
	
		XpStatus.UpdateXP()
	
		XpStatus.ChangeDisplayMode()
end

function XpStatus.resetRP()
		LevelInformation.RpPerHour   = 0
		LevelInformation.RpSession   = 0
		LevelInformation.RpToLevel   = 0
		LevelInformation.RpRank      = 0
	
		LevelInformation.TimePlayedRP  = 0.00001 -- guard against divide by zero
	
		XpStatus.UpdateRP()
	
		XpStatus.ChangeDisplayMode()
end

function XpStatus.resetGXP()
		LevelInformation.GXPPerHour  = 0
		LevelInformation.GXPSession  = 0
		LevelInformation.GXPToLevel  = 0
		LevelInformation.GXPRank     = 0
	
		LevelInformation.TimePlayedGXP  = 0.00001 -- guard against divide by zero
	
		LevelInformation.GXPFirst = GameData.Guild.m_GuildExpCurrent
	
		XpStatus.UpdateGXP()
	
		XpStatus.ChangeDisplayMode()
end

function XpStatus.resetMoney()
		LevelInformation.MoneyPerHour = 0
		LevelInformation.MoneySession = 0
		LevelInformation.GoldToLevel = 0
		LevelInformation.GoldFirst   = 0
		LevelInformation.QuotaMet    = 0
	
		LevelInformation.TimePlayedMoney  = 0.00001 -- guard against divide by zero
	
		LevelInformation.GoldFirst = GameData.Player.money
	
		XpStatus.UpdateMoney()
	
		XpStatus.ChangeDisplayMode()
end

------------------------------------------------------------------
---- Quota Window
------------------------------------------------------------------

function XpStatus.ShowQuotaWindow()
		-- Save current Gold Quota
		XpStatus.OldGoldQuota = XpStatus.Settings.GoldQuota
		
		-- Update quota text
		TextEditBoxSetText("XpStatusQuotaWindowText", towstring(XpStatus.Settings.GoldQuota))
		WindowSetShowing( "XpStatusQuotaWindow", true )
end

function XpStatus.OnQuotaTextChanged()
		
		local editInputText = TextEditBoxGetText("XpStatusQuotaWindowText")
		local outputNum = nil
		
		if( editInputText ~= nil ) then
			outputNum = tonumber(WStringToString(editInputText))
		end

    if( outputNum ~= nil) then
			XpStatus.Settings.GoldQuota = outputNum
		end

end

function XpStatus.OnSetNewQuota()

    local editInputText = TextEditBoxGetText("XpStatusQuotaWindowText")
    local outputNum = nil
    
    if( editInputText ~= nil ) then
        outputNum = tonumber(WStringToString(editInputText))
    end

    if( outputNum ~= nil) then
			XpStatus.Settings.GoldQuota = outputNum
		end

		if (XpStatus.Settings.GoldQuota ~= XpStatus.OldGoldQuota) then
			-- Output to chat log window new gold quota
			TextLogAddEntry("Chat", 0, LOC_TEXT.TEXT_NEW_GOLD_QUOTA .. XpStatus.Settings.GoldQuota .. L" " .. GetMailString( StringTables.Mail.LABEL_MAIL_COIN_GOLD ) )
		else
			-- Output to chat log window gold quota
			TextLogAddEntry("Chat", 0, LOC_TEXT.TEXT_GOLD_QUOTA .. XpStatus.Settings.GoldQuota .. L" " .. GetMailString( StringTables.Mail.LABEL_MAIL_COIN_GOLD ) )
		end

		LevelInformation.QuotaMet = 0
		XpStatus.UpdateMoney()

		WindowSetShowing( "XpStatusQuotaWindow", false )

end

function XpStatus.OnKeyEscape()
    XpStatus.OnCancelNewQuota()
end

function XpStatus.OnKeyEnter()
    XpStatus.OnNewQuota()
end

function XpStatus.OnCancelNewQuota()
		WindowSetShowing( "XpStatusQuotaWindow", false )
end


------------------------------------------------------------------
---- Mouseover Tabs
------------------------------------------------------------------

function XpStatus.OnMouseOverTab()
    local windowName	= SystemData.ActiveWindow.name
    local windowIndex	= WindowGetId (windowName)

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, XpStatus.Tabs[windowIndex + 1].tooltip ) -- add 1
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_HEADING)
    Tooltips.Finalize ()
    
    local anchor = { Point="top", RelativeTo=windowName, RelativePoint="bottom", XOffset=0, YOffset=-8 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

------------------------------------------------------------------
---- Tab Functions
------------------------------------------------------------------

-- EventHandler for OnLButtonUp when a user L-clicks a tab
function XpStatus.OnLButtonUpTab()
		local windowName	= SystemData.ActiveWindow.name
		local windowIndex	= WindowGetId (windowName)
	
		-- Don't allow the user to select the tab when disabled.
		if( ButtonGetDisabledFlag( SystemData.ActiveWindow.name ) == true ) then
  		return
 		end

		XpStatus.ChangeModeTo(windowIndex)
end

--Changes the state of the Tab Buttons
function XpStatus.SetHighlightedTab(tabNumber)

    for index, TabIndex in ipairs(XpStatus.Tabs) do
        if (index ~= tabNumber) then
            ButtonSetPressedFlag( TabIndex.window, false )
        else
            ButtonSetPressedFlag( TabIndex.window, true )
        end
    end
end
