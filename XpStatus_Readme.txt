Title: XPStatus+G v1.2

Author: Likort, Lords of the Dead, http://www.lotd.org - Original author
 				Alpha_Male (alpha_male@speakeasy.net) - Current maintainer

Description:	Displays your Experience, Renown, Guild XP, and Money earned during sessions.

Features: * Multiple selectable tabs that display Experience, Renown, Guild XP and Money information for current session
 						* Tab window for chosen stat displays the following information:
 							* Current amount for the displayed stat for this session
 							* Amount for the displayed stat till the next level (for Level, Renown Rank, or Guild Rank)
 							* Amount till set quota (for Gold) is achieved
 							* Current trending for the displayed stat, amount being gained per hour
 							* Current trending for the displayed stat, how much time until the next level, rank, or quota is reached
 							* Show duration of current session
 						* Show/Hide main window button
 						* Right-click Options Context Menu that has the following features:
						* Edit Gold Quota
						* Set Show/Hide Toggle Button Color
						* Set Window Opacity
						* Reset Stat Tabs, either individually or all of them
					* Window and toggle button can be positioned anywhere on the screen:
						* Show/Hide toggle button movable via the layouteditor
						* Main window movable in game or via the layouteditor
					* Mouseover tooltip info for each tab
					* Multiple Language Support (Need translations)

Files: \language\XpStatusLocalization.lua
			 \source\XpStatus.lua
 			 \source\XpStatus.xml
 			 \source\XpStatusWindowNub.lua
 			 \source\XpStatusWindowNub.xml
 			 \XpStatus.mod
 			 \XpStatus_Install.txt
 			 \XpStatus_Readme.txt
		
Version History: 1.2 Maintenance Update and New Features
								 		 - New versioning scheme      
 										 - Added support for 1.3.1 WARInfo Categories and Careers (XpStatus.mod)
										 - Added support for version info (XpStatus.mod)
										 - Updated mod version information for 1.4.5
										 - Directory structure reorganization - THIS REQUIRES DELETING OLD DIRECTORY WHEN INSTALLING!
										 - Localization support
										 - SavedVariables setup reworked
										 - Loading success output to chat and debug windows
										 - Right-click context menu rework
										 	 - Context menu anchor fixes
										   - Reorganized context menus and sub-menus
										 	   - More commonly used options higher in list
										 	   - Added seperators to main context menus and sub-menus
										 	   - Updated Show/Hide Button Color options
										 		   - Added a None or no tint option (default)
										 		   - Added Blue and Green options 
										 		   - Black option replaced with Dark Gray and is no longer a fallback option
												 - Added a Reset to Defaults for resetting addon windows to original size, scales, and positions
										 - Reworked Gold Quota Window and functionality
										 	 - Increased window size
										 	 - Textbox input only takes number input and backspace
										 	 - Removed negative number checks
										 	 - Added check for chat display text, takes into account whether Gold Quota is same or new
										 - Reworked Opacity window
										 	 - Increased window size to prevent text cutoff and make room for new features
										 	 - Added slider tick marks on quarter marks
										 	 - Added text values to slider tick marks
										 	 - Window opens centered on the main window
										 - Increased default Opacity value
										 - Reworked tab buttons and functionality
										 	 - The tabs now behave like ever other tab in the game
										 	 - Increased tab sizes to fit text properly
										 	 - Changed "Gold" tab title to "Money"
										 	 - Added tab seperators
										 	 - Updated Tooltips for mouseover on each tap
										 - Increased window size to match tab size increase and space things better
										 - Reworded some of the text descriptions
										 - References and displayed text changed to for consistency with regards to "money" and "gold"
										 - Removed click anywhere on window functionality that cycled through tabs
										 - Added new header file format and associated information
										 - Script cleanup and optimizations (.lua and .xml files)
										 - Script reorganization and additional comments (.lua and .xml files)
										 - Updated license information in header file to match that on Curse site (MIT license)
										 - Readme and install file additions
								 1.1.12 - Added Tabs to switch directly to each Page
								 1.1.11 - Better handling of negative Gold values
									 		  - Window movable without using Interface Editor, Lock/Unlock option
												- Changed order of context menu items
								 1.1.10 - Quota is now only compared to your Gold per Session value
												- Reset Options have been renamed to be more consistent with Tab Names
								 1.1.9  - Added Money Earned Tab. You can set a Gold Quota that you are working towards in the ContextMenu.
								 1.1.8  - You can now reset each Tab individually, as well as all Tabs
								 1.1.7b - Fixed a bug that caused the addon to stop working
								 1.1.7  - Added ContextMenu
													- Reset All
													- Change the Color of the Show/Hide Button
													- Edit Window Opacity
												- ContextMenu is activated with Right Mousebutton
									 			- Displayed Page is now being changed with Left Mousebutton
								 1.1.6	- Main Window should now be placed by default right under the Hide Button
									 			- You can toggle Hide Button Colors by Rightclicking on it
												- Changed XpStatus+G Addon Path. It's not a bug, it's a feature.
								 1.1.5  - Added Reset Button
								 1.1.4	- Added Show/Hide Button
								 1.1.3	- Minor Cosmetic Change
								 1.1.2	- Fixed XP gain in current Session display when loading into the game for the first time being displayed wrong
								 1.1.1	- Added Guild XP Page to Askanko's XpStatus WAR Addon Version 1.1

Supported Versions: Warhammer Online v1.4.5

Dependencies: None

Addon Compatability: None

Future Features: Continued updates for multiple language support.

Known Issues: None

Additional Credits: Original based on XPStatus by Askanko, http://war.curse.com/downloads/war-addons/details/xp_rp_tracker.aspx

Special Thanks:	EA/Mythic for a great game and for releasing the API specs
							  The now defunct War Wiki (www.thewarwiki.com) which was a great source of knowledge for WAR mod development
								www.curse.com and www.curseforge.com for hosting WAR mod files and projects
							  Trouble INC guild of Badlands for testing, feedback, and for their support

License/Disclaimers: This program released under the MIT License.

										 Copyright (c) 2011

										 Permission is hereby granted, free of charge, to any person obtaining a copy
										 of this software and associated documentation files (the "Software"), to deal
										 in the Software without restriction, including without limitation the rights
										 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
										 copies of the Software, and to permit persons to whom the Software is
										 furnished to do so, subject to the following conditions:
										 
										 The above copyright notice and this permission notice shall be included in
										 all copies or substantial portions of the Software.
										 
										 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
										 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
										 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
										 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
										 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
										 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
										 THE SOFTWARE.
